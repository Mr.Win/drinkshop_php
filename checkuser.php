<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:07 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/checkuser.php
 * Method:POST
 * Param:phone
 * Result:JSON
 */
require_once 'db_functions.php';
$db=new DB_Functions();
$response=array();
if (isset($_POST["phone"]))
{
    $phone=$_POST["phone"];
    if ($db->checkExistsUser($phone))
    {
        $response["exists"]=true;
        echo json_encode($response);
    }else
    {
        $response["exists"]=false;
        echo json_encode($response);
    }
}else
{
    $response["error_msg"]="Required parameter (phone) is missing!";
    echo json_encode($response);
}

?>