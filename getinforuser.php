<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/getinforuser.php
 * Method:POST
 * Param:phone,avatar,name,birthdate,address
 * Result:JSON
 */

require_once 'db_functions.php';
$db = new DB_Functions();
$response = array();
if (isset($_POST['phone'])) {

    $phone = $_POST['phone'];

    $user = $db->getUserInformation($phone);
    if ($user) {
        $response["phone"] = $user["Phone"];
        $response["avatar"]=$user["Avatar"];
        $response["name"] = $user["Name"];
        $response["birthdate"] = $user["Birthdate"];
        $response["address"] = $user["Address"];
        echo json_encode($response);
    } else {
        $response["error_msg"] = "User does not exists";
        echo json_encode($response);
    }

} else {
    $response["error_msg"] = "Required parameter (phone) is missing!";
    echo json_encode($response);
}

?>