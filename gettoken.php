<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/gettoken.php
 * Method:POST
 * Param:phone,isServerToken
 * Result:JSON
 */

require_once 'db_functions.php';
$db=new DB_Functions();

if (isset($_POST["phone"]) && isset($_POST["isServerToken"]))
{
    $phone=$_POST["phone"];
    $isServerToken=$_POST["isServerToken"];

    $result=$db->getToken($phone,$isServerToken);
    echo json_encode($result);
}else
{
    echo json_encode("Required parameter (phone,isServerToken) is missing!");
}

?>