<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/updatetoken.php
 * Method:POST
 * Param:phone,token,isServerToken
 * Result:JSON
 */

require_once 'db_functions.php';
$db=new DB_Functions();

if (isset($_POST["phone"]) &&
    isset($_POST["token"]) &&
    isset($_POST['isServerToken']))
{
    $phone=$_POST["phone"];
    $token=$_POST["token"];
    $isServerToken=$_POST['isServerToken'];

    $user=$db->insertToken($phone,$token,$isServerToken);
    if ($user)
        echo json_encode("Token update success");
    else
        echo json_encode("Token update failed");
}else
{
    $response["error_msg"]="Required parameter (phone,token,isServerToken) is missing!";
    echo json_encode($response);
}

?>