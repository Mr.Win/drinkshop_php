<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/server/order/update_order_status.php
 * Method:POST
 * Param:status,phone,OrderId
 * Result:JSON
 */

require_once '../../db_functions.php';
$db = new DB_Functions();

if (isset($_POST["status"]) &&
    isset($_POST["phone"]) &&
    isset($_POST["orderId"])) {
    $status = $_POST["status"];
    $phone = $_POST["phone"];
    $orderId = $_POST["orderId"];

    $result = $db->updateOrderStatus($phone, $orderId, $status);
    echo json_encode($result);
} else {
    echo json_encode("Required parameter (status,phone,orderId) is missing!");
}

?>