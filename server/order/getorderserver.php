<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/server/order/getorderserver.php
 * Method:POST
 * Param:status
 * Result:JSON
 */

require_once '../../db_functions.php';
$db=new DB_Functions();

if (isset($_POST["status"]))
{
    $status=$_POST["status"];

    $orders=$db->getOrderServerByStatus($status);
    echo json_encode($orders);
}else
{
    $response["error_msg"]="Required parameter (status) is missing!";
    echo json_encode($response);
}

?>