<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/30/2018
 * Time: 15:30
 */
require_once '../../db_functions.php';
$db = new DB_Functions();
if (isset($_POST['id']) )
{
    $id=$_POST['id'];
    $result=$db->deleteProduct($id);
    if ($result)
        echo json_encode("Delete product success");
    else
        echo json_encode("Error while delete to database");
}else{
    echo json_encode("Required parameters (id) is missing !");
}

?>