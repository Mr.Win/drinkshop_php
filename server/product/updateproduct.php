<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/30/2018
 * Time: 15:30
 */
require_once '../../db_functions.php';
$db = new DB_Functions();
if (isset($_POST['id']) )
{
    $id=$_POST['id'];
    $name=$_POST['name'];
    $imgPath=$_POST['imgPath'];
    $price=$_POST['price'];
    $menuId=$_POST['menuId'];
    $result=$db->updateProduct($id,$name,$imgPath,$price,$menuId);
    if ($result)
        echo json_encode("Update product success");
    else
        echo json_encode("Error while write to database");
}else{
    echo json_encode("Required parameters (id,name,imgPath,price,menuId) is missing !");
}

?>