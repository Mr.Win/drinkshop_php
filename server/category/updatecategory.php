<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/30/2018
 * Time: 15:30
 */
require_once '../../db_functions.php';
$db = new DB_Functions();
if (isset($_POST['id']) )
{
    $id=$_POST['id'];
    $name=$_POST['name'];
    $imgPath=$_POST['imgPath'];
    $result=$db->updateCategory($id,$name,$imgPath);
    if ($result)
        echo json_encode("Update category success");
    else
        echo json_encode("Error while write to database");
}else{
    echo json_encode("Required parameters (id,name,imgPath) is missing !");
}

?>