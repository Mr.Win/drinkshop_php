<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/getdrink.php
 * Method:POST
 * Param:menuid
 * Result:JSON
 */

require_once 'db_functions.php';
$db=new DB_Functions();
$response=array();
if (isset($_POST["menuid"]))
{
    $menuid=$_POST["menuid"];
    $drinks=$db->getDrinkByMenuid($menuid);
    echo json_encode($drinks);
}else
{
    $response["error_msg"]="Required parameter (menuid) is missing!";
    echo json_encode($response);
}

?>