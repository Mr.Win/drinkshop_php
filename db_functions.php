<?php

/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 10:23 AM
 */
class DB_Functions
{
    private $conn;


    function __construct()
    {
        require_once 'db_connect.php';
        $db = new DB_Connnect();
        $this->conn = $db->connect();
    }

    function __destruct()
    {
        //TODO implement _destruct method
    }

    /**
     * @param $phone
     * @return bool
     */
    public function checkExistsUser($phone)
    {
        /*
         * check user exist
         * return true/false
         */
        $stmt = $this->conn->prepare("SELECT Phone FROM user WHERE Phone = ?");
        $stmt->bind_param('s', $phone);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function registerNewUser($phone, $name, $birthdate, $address)
    {
        /*
         * register new user
         * return user if user was created
         * return error message if have exception
         */
        $stmt = $this->conn->prepare("INSERT INTO user (Phone,Name,Birthdate,Address) VALUES (?,?,?,?)");
        $stmt->bind_param('ssss', $phone, $name, $birthdate, $address);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM user WHERE Phone = ?");
            $stmt->bind_param('s', $phone);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return false;
        }
    }

    public function getUserInformation($phone)
    {
        /*
         * get information user
         * return user if user was existed
         * return error message if not existed
         */
        $stmt = $this->conn->prepare("SELECT * FROM user WHERE Phone = ?");
        $stmt->bind_param('s', $phone);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return false;
        }
    }

    public function getBanners()
    {
        /*
         * get banners
         * return list banners
         * */
        $result = $this->conn->query("SELECT * FROM banner ORDER BY ID LIMIT 4");
        $banners = array();
        while ($item = $result->fetch_assoc())
            $banners[] = $item;
        return $banners;
    }
    public function getMenu()
    {
        /*
         * get menu
         * return list menu
         * */
        $result = $this->conn->query("SELECT * FROM menu ");
        $menu = array();
        while ($item = $result->fetch_assoc())
            $menu[] = $item;
        return $menu;
    }
    public function getDrinkByMenuid($menuId)
    {
        /*
         * get drinks by menuid
         * return list drinks
         * */
        $result = $this->conn->query("SELECT * FROM drink WHERE Menuid='".$menuId."'");
        $drink = array();
        while ($item = $result->fetch_assoc())
            $drink[] = $item;
        return $drink;
    }
    public function uploadAvatar($phone,$filename)
    {
        /*
         * upload avatarurl
         * return TRUE or FALSE
         * */
        return $result = $this->conn->query("UPDATE user SET  Avatar='$filename' WHERE Phone='$phone'");

    }
    public function getAllDrinks()
    {
        /*
         * get All drinks for search bar
         * return list drink or empty
         * */
         $result = $this->conn->query("SELECT * FROM drink WHERE 1") or die($this->conn->error);
         $drinks=array();
         while ($item=$result->fetch_assoc())
             $drinks[]=$item;
         return $drinks;

    }
    public function insertNewOrder($orderPrice,$orderComment,$orderAddress,$orderDetail,$userPhone,$paymentMethod)
    {
        /*
         * insert new order
         * */
       $stmt=$this->conn->prepare("INSERT INTO `oder`(`OrderDate`,`OderStatus`, `OderPrice`, `OderDetail`, `OderComment`, `OderAddress`, `UserPhone`,`PaymentMethod`) VALUES (NOW(),0,?,?,?,?,?,?)")
       or die($this->conn->error);
       $stmt->bind_param("ssssss",$orderPrice,$orderDetail,$orderComment,$orderAddress,$userPhone,$paymentMethod);
       $result=$stmt->execute();
       $stmt->close();
       if ($result)
       {
           $stmt=$this->conn->prepare("SELECT * FROM `oder` WHERE `UserPhone`=? ORDER BY OrderId DESC LIMIT 1")
           or die($this->conn->error);
           $stmt->bind_param("s",$userPhone);
           $stmt->execute();
           $order=$stmt->get_result()->fetch_assoc();
           $stmt->close();
           return $order;
       }
       else
           return false;
    }
    public function insertNewCategory($name,$imgPath)
    {
        /*
         * insert new menu(category)
         * */
        $stmt=$this->conn->prepare("INSERT INTO `menu`(`Name`, `Link`) VALUES (?,?)")
        or die($this->conn->error);
        $stmt->bind_param("ss",$name,$imgPath);
        $result=$stmt->execute();
        $stmt->close();
        if ($result)
            return true;
        else
            return false;
    }
    public function updateCategory($id,$name,$imgPath)
    {
        /*
         * update category
         * return true or false
         * */
        $stmt=$this->conn->prepare("UPDATE  `menu` SET `Link`=?,`Name`=? WHERE `ID`=?");
        $stmt->bind_param("sss",$imgPath,$name,$id);
        $result=$stmt->execute();
        $stmt->close();
        return $result;
    }
    public function deleteCategory($id)
    {
        /*
         * update category
         * return true or false
         * */
        $stmt=$this->conn->prepare("DELETE FROM  `menu`  WHERE `ID`=?");
        $stmt->bind_param("s",$id);
        $result=$stmt->execute();
        $stmt->close();
        return $result;
    }
    public function insertNewDrink($name,$imgPath,$price,$menuId)
    {
        /*
         * insert new drink(product)
         * */
        $stmt=$this->conn->prepare("INSERT INTO `drink`(`Name`, `Link`,`Price`,`MenuId`) VALUES (?,?,?,?)")
        or die($this->conn->error);
        $stmt->bind_param("ssss",$name,$imgPath,$price,$menuId);
        $result=$stmt->execute();
        $stmt->close();
        if ($result)
            return true;
        else
            return false;
    }
    public function updateProduct($id,$name,$imgPath,$price,$menuId)
    {
        /*
         * update product
         * return true or false
         * */
        $stmt=$this->conn->prepare("UPDATE  `drink` SET `Link`=?,`Name`=?, `Price`=?, `MenuId`=? WHERE `ID`=?");
        $stmt->bind_param("sssss",$imgPath,$name,$price,$menuId,$id);
        $result=$stmt->execute();
        $stmt->close();
        return $result;
    }
    public function deleteProduct($id)
    {
        /*
         * delete product
         * return true or false
         * */
        $stmt=$this->conn->prepare("DELETE FROM  `drink`  WHERE `ID`=?");
        $stmt->bind_param("s",$id);
        $result=$stmt->execute();
        $stmt->close();
        return $result;
    }
    public function getOrderByStatus($userPhone,$status)
    {
        /*
         * get all order base on userphone and status*/
        $query="SELECT * FROM `oder` WHERE `OderStatus`='".$status."' AND `UserPhone`='".$userPhone."'";
        $result=$this->conn->query($query) or die($this->conn->error);
        $orders=array();
        while ($order=$result->fetch_assoc())
            $orders[]=$order;
        return $orders;

    }
    public function getOrderServerByStatus($status)
    {
        /*
         * get all order base on status*/
        $query="SELECT * FROM `oder` WHERE `OderStatus`='".$status."'";
        $result=$this->conn->query($query) or die($this->conn->error);
        $orders=array();
        while ($order=$result->fetch_assoc())
            $orders[]=$order;
        return $orders;

    }
    public function insertToken($phone,$token,$isServerToken)
    {
        /*
         * INSERT OR UPDATE TOKEN
         * return Token object or FALSE*/
        $stmt=$this->conn->prepare("INSERT INTO token(Phone,Token,isSerVerToken) VALUES (?,?,?) ON DUPLICATE KEY UPDATE Token=?,isSerVerToken=?")
            or die($this->conn->error);
        $stmt->bind_param("sssss",$phone,$token,$isServerToken,$token,$isServerToken);
        $result=$stmt->execute();
        $stmt->close();

        //check for successful store
        if ($result)
        {
            $stmt=$this->conn->prepare("SELECT * FROM token WHERE Phone=?");
            $stmt->bind_param("s",$phone);
            $stmt->execute();
            $user=$stmt->get_result()-> fetch_assoc();
            $stmt->close();
            return $user;
        }else
        {
            return false;
        }
    }
    public function cancelOrder($orderId,$userPhone)
    {
        /*
         * CANCEL order
         * return true or false*/
        $stmt=$this->conn->prepare("UPDATE `oder` SET `OderStatus`=-1 WHERE `OrderId`=? AND UserPhone=?")
        or die($this->conn->error);
        $stmt->bind_param("ss",$orderId,$userPhone);
        $result=$stmt->execute() or die($stmt->error);
        $stmt->close();
        return $result;
    }
    public function getNearbystore($lat,$lng)
    {
        /*
         * get nearby store
         * return list store or false*/
        $result=$this->conn->query("SELECT id,name,lat,lng, ROUND(117.045 * DEGREES(ACOS(COS(RADIANS($lat))"
        ."* COS(RADIANS(lat))"
            ."* COS(RADIANS(lng) - RADIANS($lng))"
            ."+ SIN(RADIANS($lat))"
            ."* SIN(RADIANS(lat)))),2)"
            ." AS distance_in_km FROM store"
            ." ORDER BY distance_in_km ASC") or die($this->conn->error);
        $stores=array();
        while ($store=$result->fetch_assoc())
            $stores[]=$store;
        return $stores;
    }
    public function updateOrderStatus($phone,$orderId,$status)
    {
        /*
         * server app update order
         * return result or false*/
        $stmt=$this->conn->prepare("UPDATE `oder` SET `OderStatus`= ? WHERE `OrderId`= ? AND `UserPhone`= ?")
            or die($this->conn->error);
        $stmt->bind_param("sss",$status,$orderId,$phone);
        $result=$stmt->execute();
        $stmt->close();
        return $result;
    }
    public function getToken($phone,$isServerToken)
    {
        /*
         * get token
         * return result or false*/
        $stmt=$this->conn->prepare("SELECT * FROM `token` WHERE Phone=? AND isSerVerToken=?")
        or die($this->conn->error);
        $stmt->bind_param("ss",$phone,$isServerToken);
        $result=$stmt->execute();
        $token=$stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $token;
    }
}
?>