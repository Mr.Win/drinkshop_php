<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/getbanners.php
 * Method:POST
 * Param:
 * Result:JSON
 */

require_once 'db_functions.php';
$db = new DB_Functions();

$banners=$db->getBanners();
echo json_encode($banners);

?>