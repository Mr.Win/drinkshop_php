<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/getmenu.php
 * Method:POST
 * Param:menu
 * Result:JSON
 */

require_once 'db_functions.php';
$db = new DB_Functions();

$menu=$db->getMenu();
echo json_encode($menu);

?>