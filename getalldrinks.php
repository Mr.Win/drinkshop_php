<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/getalldrinks.php
 * Method:POST
 * Param:menuid
 * Result:JSON
 */

require_once 'db_functions.php';
$db = new DB_Functions();
$response = array();

$drinks = $db->getAllDrinks();
if ($drinks)
    echo json_encode($drinks);
else
    echo json_encode("error !")

?>