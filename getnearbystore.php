<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 11/10/2018
 * Time: 15:36
 */
require_once "db_functions.php";
$db = new DB_Functions();
if (isset($_POST["lat"]) &&
    isset($_POST["lng"])) {

    $lat=$_POST["lat"];
    $lng=$_POST["lng"];

    $stores=$db->getNearbystore($lat,$lng);
    if ($stores)
        echo json_encode($stores);
    else
        echo json_encode("Store is not exists");
}else
{
    echo json_encode("Required field (lat,lng) is missing !");
}
?>