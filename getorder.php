<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/getorder.php
 * Method:POST
 * Param:userPhone,status
 * Result:JSON
 */

require_once 'db_functions.php';
$db=new DB_Functions();

if (isset($_POST["userPhone"]) && isset($_POST["status"]))
{
    $userPhone=$_POST["userPhone"];
    $status=$_POST["status"];

    $orders=$db->getOrderByStatus($userPhone,$status);
    echo json_encode($orders);
}else
{
    $response["error_msg"]="Required parameter (userPhone,status) is missing!";
    echo json_encode($response);
}

?>