<?php
/**
 * Created by PhpStorm.
 * User: vanthang
 * Date: 10/4/2018
 * Time: 11:22 AM
 */
/*
 * Endpoint:http://<domain>/drinkshop/submitorder.php
 * Method:POST
 * Param:orderPrice,orderDetail,orderComment,orderAddress,userPhone
 * Result:JSON
 */

require_once 'db_functions.php';
$db = new DB_Functions();
$response = array();
if (isset($_POST['userPhone'])&&
    isset($_POST['orderDetail']) &&
    isset($_POST['orderComment']) &&
    isset($_POST['orderAddress']) &&
    isset($_POST['orderPrice']) &&
    isset($_POST['paymentMethod'])
    )
{

    $phone = $_POST['userPhone'];
    $orderDetail = $_POST['orderDetail'];
    $orderAddress= $_POST['orderAddress'];
    $orderComment = $_POST['orderComment'];
    $orderPrice = $_POST['orderPrice'];
    $paymentMethod=$_POST['paymentMethod'];

    $result=$db->insertNewOrder($orderPrice,$orderComment,$orderAddress,$orderDetail,$phone,$paymentMethod);
    echo json_encode($result);

} else {
    echo json_encode("Required parameter (phone,detail,address,comment,price,paymentMethod) is missing!");
}

?>